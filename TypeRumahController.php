<?php

namespace App\Http\Controllers;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\TypeRumah;
use Illuminate\Support\Facades\DB;
use PDF;
class TypeRumahController extends Controller
{
    //penambahan public function index(Request $request) <- Digunakan untuk search engine secara global
    public function index(){
        
        $type = TypeRumah::get();
        return view('type.index', ["type" => $type]);
    }


    // public function search(Request $request){
    //     $search = $request->search;
    //     $pencatatan = DB::table('type_rumah')->where('nama_customer','LIKE',"%".$search."%");
    //     return view('type.index', ["type" => $type]);
    // }

    public function store(Request $request){
        // dd($request->all());
        // DB::table('type_rumah')->insert([
        //     "nama_rumah" => $request->nama_rumah,
        //     "luas_rumah" => $request->nama_type,
        //     "nama_type" => $request->luas_rumah
        // ]);

        TypeRumah::create([
            "luas_rumah" =>$request->luas_rumah,
            "nama_rumah" =>$request->nama_rumah,
            "nama_customer" =>$request->nama_customer,
            "nama_type" =>$request->nama_type,
            "no_telp" =>$request->no_telp,
            "nik" =>$request->nik,
            "status" =>$request->status,
            "alamat" =>$request->alamat,
            "ltrumah" =>$request->ltrumah,
            "kategori_perumahan" =>$request->kategori_perumahan,
            "hari" =>$request->hari,
            "harga_rumah" =>$request->harga_rumah
        ]);

        return redirect()->route('pencatatan.index');
    }

    public function update(Request $request, $id){
        // dd($request->status());
        DB::table('type_rumah')->where('id_type', $id)->update([
            "luas_rumah" =>$request->edluas_rumah,
            "nama_rumah" =>$request->ednama_rumah,
            "nama_customer" =>$request->ednama_customer,
            "nama_type" =>$request->ednama_type,
            "no_telp" =>$request->edno_telp,
            "status" =>$request->edstatus,
            "nik" =>$request->ednik,
            "alamat" =>$request->edalamat,
            "ltrumah" =>$request->edltrumah,
            "kategori_perumahan" =>$request->edkategori_perumahan,
            "hari" =>$request->edhari,
            "harga_rumah" =>$request->edharga_rumah
        ]);
        return redirect()->route('pencatatan.index');
    }

    public function destroy($id){
        DB::table('type_rumah')->where('id_type', $id)->delete();
        return redirect()->route('pencatatan.index');
    }

    public function cetak_pdf(){
        $type = TypeRumah::all();
        $pdf = PDF::loadview('typerumah_pdf',['TypeRumah'=>$type]);
        return $pdf->download(pencatatan-data-pdf);
    }
}
