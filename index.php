@extends('templates.default')
@section('contentPages')
<!-- Begin Page Content -->
<nav aria-label="breadcrumb">
    <ol class="breadcrumb  float-right">
        <li class="breadcrumb-item"><a href={{ route('dashboard') }}>Dashboard</a></li>
        <li class="breadcrumb-item active" aria-current="page">Pencatatan</li>
    </ol>
</nav>
<div class="container-fluid">
    <!-- Page Heading -->
    <h1 class="h3 mb-2 text-gray-600">Tabel Pencatatan Progses Pengerjaan Rumah</h1>
    <p class="mb-4">Aplikasi progres pencatatan pengerjaan rumah <b>PT. Anugrah Putra Kharisma</b>.</p>
    {{-- <!-- ============================================================== -->
    <!-- INFORMATION NOTICE -->
    <div class="information-notice">
        <div class="information-title">
            <p class="information-child">
                <li>Untuk <b><u> mengubah atau mengedit</u></b> data inputan dapat menggunakan icon <i class="far fa-edit"></i> <b>(Edit)</b></li>
                <li>Untuk <b><u>menghapus data</u></b>, dapat klik icon <i class="fas fa-trash"></i> <b> (Delete / Hapus)</b></li>
                </li>
        </div>
    </div>
    <!-- END OF INFORMATION NOTICE -->
    <!-- ============================================================== -->  --}}

    <button type="button" class="btn btn-primary btn-sps shadow mb-4" data-toggle="modal" data-target="#tambahForm"><i class="fas fa-plus"></i>
        Tambah Data
    </button>

    <!-- DataTales Example -->
    <div class="card shadow mb-4">
        <div class="card-header py-1">
            <div class="d-flex bd-highlight">
                <div class="p-3 flex-grow-1 bd-highlight">
                    <h6 class="m-0 font-weight-bold text-primary">Aplikasi Pencatatan Progres Pengerjaan Rumah PT. Anugrah Putra Kharisma</h6>
                </div>
                <!-- PDF BUTTON PRINT -->
                <div class="p-2 bd-highlight"><a href="#" onclick="" data-toggle="modal" class="btn btn-sm">
                        <i class="fas fa-file-pdf"></i>&nbsp; CetakPDF</a></div>
            </div>
        </div>
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr>
                            <th style="width:3%;">No</th>
                            <th style="width:2rem;">Luas Rumah</th>
                            <th style="width:12rem;">Cluster Rumah</th>
                            <th style="width:12rem;">Kategori Perumahan</th>
                            <th style="width:18rem;">Nama Customer</th>
                            <th style="width:360px;">Alamat Customer</th>
                            <th>No. Telp Customer</th>
                            <th style="width:14rem;">NIK Customer</th>
                            <th style="width:8rem;">Status Pembangunan</th>
                            <th style="width:20rem;">Estimasi Waktu</th>
                            <th style="width:2rem;">Tipe Rumah</th>
                            <th style="width:2rem;">Lantai Rumah</th>
                            <th style="width:25rem;">Harga Rumah</th>
                            <th style="width:25rem;">
                                <center style="width:8rem;">Aksi</center>
                            </th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                      $no = 1;
                      ?>
                        @foreach($type as $row)
                        <tr>
                            <td>{{ $no++ }}</td>
                            <td>{{ $row->luas_rumah }}</td>
                            <td>{{ $row->nama_rumah }}</td>
                            <td>{{ $row->kategori_perumahan }}</td>
                            <td>{{ $row->nama_customer }}</td>
                            <td>{{ $row->alamat }}</td>
                            <td>{{ $row->no_telp }}</td>
                            <td>{{ $row->nik }}</td>
                            <td>{{ $row->status }}</td>
                            <td>{{ $row->hari }}</td>
                            <td>{{ $row->nama_type }}</td>
                            <td>{{ $row->ltrumah }}</td>
                            <td>{{ 'Rp '.number_format($row->harga_rumah, 2, ",", ".") }}</td>
                            <td>
                                <center>
                                    {{-- BUTTON EDIT START --}}
                                    <button type="button" class="btn btn-primary btn-sm" id="edit" name="edit" href="{{ route('pencatatan.update', $row->id_type) }}" data-nama="{{ $row->nama_rumah }}" data-nama_customer="{{ $row->nama_customer }}" data-luas="{{ $row->luas_rumah }}" data-harga="{{ $row->harga_rumah }}" data-no_telp="{{ $row->no_telp }}" data-nik="{{ $row->nik }}" data-status="{{ $row->status }}" data-hari="{{ $row->hari }}" data-alamat="{{ $row->alamat }}" data-ltrumah="{{ $row->ltrumah }}" data-kategori_perumahan="{{ $row->kategori_perumahan}}" data-tipe="{{ $row->nama_type }}"><i class="far fa-edit"></i> Edit</button>
                                    {{-- END BUTTON EDIT --}}

                                    {{-- BUTTON DELETE START --}}
                                    {{-- <button type="button" class="btn btn-danger btn-sm" id="delete" name="delete" href="{{ route('pencatatan.destroy', $row->id_type) }}"><i class="fas fa-trash"></i> Hapus --}}
                                    {{-- ENDBUTTONDELETE --}}

                                    <a href="#modalDelete" onclick="" data-toggle="modal" class="btn btn-danger btn-sm">
                                        <i class="fas fa-trash"></i> Hapus
                                    </a>
                                </center>
                                </a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <!-- Button trigger modal -->

    <!-- Modal Alert Delete -->
    <div class="modal fade" id="modalDelete" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title txt-changes" id="staticBackdropLabel">Yakin ingin menghapus ?</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                {{-- <div class="modal-body">
            Apakah ingin melanjutkan ?
          </div> --}}
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary float-left" data-dismiss="modal">
                        <small><i class="fas fa-times"></i></small> Tidak</button>
                    <button type="button" class="btn btn-danger" id="delete" name="delete" href="{{ route('pencatatan.destroy', $row->id_type) }}">
                        <small><i class="fas fa-trash"></i></small> Hapus
                </div>
            </div>
        </div>
    </div>
    <!-- End Modal Alert Delete -->

    <!-- Modal Insert -->
    <form class="needs-validation" novalidate action="{{ route('pencatatan.store') }}" method="post">
        {{ csrf_field() }}
        <div class="modal fade" id="tambahForm" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="tambahFormLabel" aria-hidden="true">
            <div class="modal-dialog modal-xl">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="tambahFormLabel"><b>PT. Anugrah Putra Kharisma</b></h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <!-- ============================================================== -->
                    <div class="modal-body">
                        <form>
                            <div class="container">
                                <div class="form-group row">
                                    <label for="nama_rumah" class="col-sm-3 col-form-label">Cluster Rumah</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="nama_rumah" name="nama_rumah" value="">
                                            <option selected hidden>- Pilih Cluster</option>
                                            <option>-</option>
                                            <option>Al-Fath</option>
                                            <option>Al-Thaf </option>
                                            <option>Al-Hafidz </option>
                                            <option>Al-Mabrur </option>
                                            <option>Istana Villa Batu </option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="kategori_perumahan" class="col-sm-3 col-form-label">Kategori Perumahan</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="kategori_perumahan" name="kategori_perumahan" value="">
                                            <option selected hidden>- Pilih Kategori Perumahan</option>
                                            <option>-</option>
                                            <option>Istana Ketapang 1 - Sidoarjo</option>
                                            <option>Istana Ketapang 2 - Sidoarjo</option>
                                            <option>Istana Wage 1 - Sidoarjo </option>
                                            <option>Istana Wage 2 - Sidoarjo </option>
                                            <option>Istana Karangbong - Sidoarjo </option>
                                            <option>Istana Villa Batu - Malang</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nama_customer" class="col-sm-3 col-form-label">Nama Customer</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="nama_customer" value="" name="nama_customer" placeholder="Nama Customer" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="no_telp" class="col-sm-3 col-form-label">No. Telephone</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="no_telp" value="" name="no_telp" placeholder="No. Telephone" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nik" class="col-sm-3 col-form-label"> NIK</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="nik" value="" name="nik" placeholder="NIK" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="luas-rumah" class="col-sm-3 col-form-label">Luas Rumah</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="luas-rumah" value="" name="luas_rumah" placeholder="60M2" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="nama_type" class="col-sm-3 col-form-label">Tipe Rumah</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="nama_type" value="" name="nama_type" placeholder="74/72" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="ltrumah" class="col-sm-3 col-form-label">Lantai Rumah</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="ltrumah" value="" name="ltrumah" placeholder="Lantai Rumah" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="status" class="col-sm-3 col-form-label">Status Pembangunan</label>
                                    <div class="col-sm-9">
                                        <select class="form-control" id="status" name="status">
                                            <option selected hidden>- Pilih Status Proses Pengerjaan</option>
                                            <option>-</option>
                                            <option>Dalam Proses</option>
                                            <option>Direncanakan</option>
                                            <option>Delay</option>
                                            <option>Jadwal Ulang</option>
                                            <option>Cancel</option>
                                            <option>Selesai</option>
                                        </select>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="hari" class="col-sm-3 col-form-label"> Hari</label>
                                    <div class="col-sm-9">
                                        <input type="text" class="form-control" id="hari" value="" name="hari" placeholder="Hari" required>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="harga_rumah" class="col-sm-3 col-form-label">Harga Rumah</label>
                                    <div class="col-sm-9">
                                        <input type="number" class="form-control" id="harga_rumah" value="" name="harga_rumah" placeholder="Harga Rumah" required>
                                        <small class="form-text text-muted">Tulis tanpa menggunakan <b>'Rp'</b> dan Tanda <b>Titik ( . )</b></small>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="alamat" class="col-sm-3 col-form-label">Alamat Customer</label>
                                    <div class="col-sm-9">
                                        <textarea class="form-control" id="alamat" value="" name="alamat" placeholder="Alamat" required></textarea>
                                        <div class="invalid-feedback">
                                            Data belum diisi !
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- ============================================================== -->
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-primary">Tambahkan</button>
                    </div>
                </div>
            </div>
        </div>
    </form>
    {{-- END MODAL INSERT --}}
    <!-- ============================================================== -->

    <!-- Modal Update -->
    <div class="modal fade" id="updateForm" name="updateForm" data-backdrop="static" data-keyboard="false" tabindex="-1" aria-labelledby="updateFormLabel" aria-hidden="true">
        <div class="modal-dialog modal-xl">
            <div class="modal-content">
                <div class="modal-header">
                    <img src="{{ asset('img/APK_LOGO.png') }}" class="rounded mr-2" alt="..." height="25px">
                    <h5 class="modal-title customer-edit" id="updateFormLabel"><b>Edit Data Customer</b></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <!-- ============================================================== -->
                <form class="needs-validation" id="updateAA" name="updateAA" novalidate action="" method="POST">
                    {{ csrf_field() }}
                    {{ method_field('PUT') }}
                    <div class="modal-body">
                        <div class="form-row">
                            <div class="col-md-6 mb-3">
                                <label for="ednama_rumah">Cluster Rumah</label>
                                <select class="form-control" id="ednama_rumah" name="ednama_rumah" value="">
                                    <option selected hidden>- Pilih Cluster</option>
                                    <option>-</option>
                                    <option>Al-Fath</option>
                                    <option>Al-Thaf </option>
                                    <option>Al-Hafidz </option>
                                    <option>Al-Mabrur </option>
                                    <option>Istana Villa Batu </option>
                                </select>
                                <div class="invalid-feedback">
                                    Data belum diisi !
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="edkategori_perumahan">Kategori Perumahan</label>
                                <select class="form-control" id="edkategori_perumahan" name="edkategori_perumahan" value="">
                                    <option selected hidden>- Pilih Kategori Perumahan</option>
                                    <option>-</option>
                                    <option>Istana Ketapang 1 - Sidoarjo</option>
                                    <option>Istana Ketapang 2 - Sidoarjo</option>
                                    <option>Istana Wage 1 - Sidoarjo </option>
                                    <option>Istana Wage 2 - Sidoarjo </option>
                                    <option>Istana Karangbong - Sidoarjo </option>
                                    <option>Istana Villa Batu - Malang</option>
                                </select>
                                <div class="invalid-feedback">
                                    Data belum diisi !
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="ednama_customer">Nama Customer</label>
                                <input type="text" class="form-control" id="ednama_customer" value="" name="ednama_customer" required>
                                <div class="invalid-feedback">
                                    Data belum di isi !
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="edno_telp">No. Telephone</label>
                                <input type="number" class="form-control" id="edno_telp" value="" name="edno_telp" required>
                                <div class="invalid-feedback">
                                    Data belum di isi !
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="ednik">NIK</label>
                                <input type="number" class="form-control" id="ednik" value="" name="ednik" required>
                                <div class="invalid-feedback">
                                    Data belum di isi !
                                </div>
                            </div>
                            <div class="col-md-6 mb-3">
                                <label for="edltrumah">Lantai Rumah</label>
                                <input type="text" class="form-control" id="edltrumah" value="" name="edltrumah" required>
                                <div class="invalid-feedback">
                                    Data belum di isi !
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="col-md-6 mb-3">
                                    <label for="edharga_rumah">Harga Rumah</label>
                                    <input type="number" class="form-control" id="edharga_rumah" value="" name="edharga_rumah" required>
                                    <small class="form-text text-muted">Tulis tanpa menggunakan 'Rp' dan Titik ( . )</small>
                                    <div class="invalid-feedback">
                                        Data belum di isi !
                                    </div>
                                </div>
                                <div class="col-md-2 mb-3">
                                    <label for="ednama_type">Tipe Rumah</label>
                                    <input type="text" class="form-control" id="ednama_type" value="" name="ednama_type" required>
                                    <div class="invalid-tooltip">
                                        Data belum di isi.
                                    </div>
                                </div>
                                <div class="col-md-2 mb-2">
                                    <label for="edluas_rumah">Luas Rumah</label>
                                    <input type="text" class="form-control" id="edluas_rumah" value="" name="edluas_rumah" required>
                                    <div class="invalid-tooltip">
                                        Data belum di isi.
                                    </div>
                                </div>
                                <div class="col-md-2 mb-2">
                                    <label for="ednama_type">Status </label>
                                    <select class="form-control" id="edstatus" name="edstatus" required>
                                        <option selected hidden>- Pilih Status Proses Pengerjaan</option>
                                        <option>-</option>
                                        <option>Dalam Proses</option>
                                        <option>Direncanakan</option>
                                        <option>Delay</option>
                                        <option>Jadwal Ulang</option>
                                        <option>Cancel</option>
                                        <option>Selesai</option>
                                    </select>
                                    <div class="invalid-tooltip">
                                        Data belum di isi.
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="edhari">Hari</label>
                                    <input type="text" class="form-control" id="edhari" value="" name="edhari" required>
                                    <div class="invalid-feedback">
                                        Data belum di isi !
                                    </div>
                                </div>
                                <div class="col-md-12 mb-3">
                                    <label for="edalamat">Alamat Customer</label>
                                    <textarea class="form-control" id="edalamat" value="" name="edalamat" required></textarea>
                                    <div class="invalid-feedback">
                                        Data belum di isi !
                                    </div>
                                </div>
                                <!-- Avoid Here -->
                                <div class="form-group">
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- ============================================================== -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
                        <button type="submit" class="btn btn-stg btn-primary">Update</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
</div>
{{-- END MODAL UPDATE --}}
<form action="" method="POST" id="deleteForm">
    {{ csrf_field() }}
    {{ method_field('DELETE') }}
</form>
<!-- /.container-fluid -->
@endsection
@push('scripts')
<script>
    // Example starter JavaScript for disabling form submissions if there are invalid fields
    (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Fetch all the forms we want to apply custom Bootstrap validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
                form.addEventListener('submit', function(event) {
                    if (form.checkValidity() === false) {
                        event.preventDefault();
                        event.stopPropagation();
                    }
                    form.classList.add('was-validated');
                }, false);
            });
        }, false);
    })();

    $('button#edit').on('click', function() {
        var href = $(this).attr("href");
        var nama = $(this).data("nama");
        var nama_customer = $(this).data("nama_customer");
        var luas = $(this).data("luas");
        var tipe = $(this).data("tipe");
        var harga = $(this).data("harga");
        var status = $(this).data("status");
        var no_telp = $(this).data("no_telp");
        var nik = $(this).data("nik");
        var alamat = $(this).data("alamat");
        var hari = $(this).data("hari");
        var ltrumah = $(this).data("ltrumah");
        var kategori_perumahan = $(this).data("kategori_perumahan");
        $('#ednama_rumah').val(nama);
        $('#ednama_customer').val(nama_customer);
        $('#edno_telp').val(no_telp);
        $('#edstatus').val(status);
        $('#edhari').val(hari);
        $('#edluas_rumah').val(luas);
        $('#ednama_type').val(tipe);
        $('#edalamat').val(alamat);
        $('#ednik').val(nik);
        $('#edharga_rumah').val(harga);
        $('#edkategori_perumahan').val(kategori_perumahan);
        $('#edltrumah').val(ltrumah);
        $('#updateAA').attr('action', href);
        $('#updateForm').modal('show');
    });

    $('button#delete').on('click', function() {
        var href = $(this).attr('href');
        $('#deleteForm').attr('action', href);
        $('#deleteForm').submit();
    });

</script>

<script>
    $('.alert').alert()

</script>

<!-- PAGINATION -->
<script>
    <script src="https://code.jquery.com/jquery-3.3.1.min.js">

</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs.popper.js/1.14.7/umd/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min js" integrity="sha384-B4gt1jrGC7Jh4AgTPSdUtOBvfO8shuf57BaghqFfPlYxofvL8/KUEfYiJOMMV+rV" crossorigin="anonymous"></script>

<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<!-- END PAGINATION -->

<script>
    $(document).ready(function() {
        $('#dataTable').DataTable();
    });

</script>


@endpush
