<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Rumah;
use Illuminate\Support\Facades\DB;
class RumahController extends Controller
{
    public function index(){
        $type = Rumah::get();
        return view('catatRumahFolder.pencatatan-rumah', ["type" => $type]);

        Rumah::create([
            "kd_rumah"=>$request->kd_rumah,
            "nm_rumah"=>$request->nm_rumah,
            "ket"=>$request->ket,
            "alamat"=>$request->alamat,
            "lantai"=>$request->lantai,
            "nr_rumah"=>$request->nr_rumah,
            "bk_rumah"=>$request->bk_rumah
        ]);

        return redirect()->route('pencatatan-rumah');
    }

    public function update(Request $request, $id){
        // dd($request->all());
        DB::table('rumah')->where('id_rumah', $id)->update([
            "kd_rumah"=>$request->edkd_rumah,
            "nm_rumah"=>$request->ednm_rumah,
            "ket"=>$request->edket,
            "alamat"=>$request->edalamat,
            "lantai"=>$request->edlantai,
            "nr_rumah"=>$request->ednr_rumah,
            "bk_rumah"=>$request->edbk_rumah
        ]);
        return redirect()->route('pencatatan-rumah');
    }

    public function destroy($id){
        DB::table('rumah')->where('id_rumah', $id)->delete();
        return redirect()->route('pencatatan-rumah');
    }
    
}
